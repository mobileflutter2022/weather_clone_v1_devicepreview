import 'package:flutter/material.dart';
import './info1.dart';
import './info2.dart';

// void main() {
//   runApp(const MyApp());
// }

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(),
      ),
    );
  }
}

Widget MyHomePage() {
  return Container(
    constraints: BoxConstraints.expand(),
    decoration: BoxDecoration(
      image: DecorationImage(
          image: AssetImage('assets/images/BG_rain.gif'), fit: BoxFit.cover),
    ),
    child: Scaffold(
      backgroundColor: Colors.transparent,
      extendBodyBehindAppBar: false,
      appBar: appBar(),
      body: ListView(
        children: [info1(), forecast(), info2()],
      ),
    ),
  );
}

Widget forecast() {
  return Padding(
    padding: const EdgeInsets.only(top: 22, bottom: 22, left: 25, right: 25),
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
        fixedSize: Size(50, 50),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        backgroundColor: Color.fromARGB(212, 183, 183, 183),
      ),
      onPressed: () {},
      child: Text(
        '5-day forecast',
        style: TextStyle(fontSize: 20),
      ),
    ),
  );
}

AppBar appBar() {
  return AppBar(
    leading: IconButton(
      onPressed: () {},
      icon: Padding(
        padding: const EdgeInsets.all(13.0),
        child: Icon(Icons.add_rounded, size: 27),
      ),
    ),
    centerTitle: true,
    title: Padding(
      padding: const EdgeInsets.only(top: 13),
      child: Text('Bangsean', style: TextStyle(fontSize: 22)),
    ),
    actions: [
      Padding(
        padding: const EdgeInsets.all(13.0),
        child: IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.more_vert_rounded,
            size: 27,
          ),
        ),
      ),
    ],
    backgroundColor: Colors.transparent,
    elevation: 0,
  );
}
