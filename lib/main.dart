import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:lecture_layout/MyApp.dart';

import './MyApp.dart';

void main() {
  runApp(const ExampleApp());
}

class ExampleApp extends StatelessWidget {
  const ExampleApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DevicePreview(
      tools: const [
        DeviceSection(),
      ],
      builder: (context) => MyApp(),
    );
  }
}
